// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

contract AxirTokenV2 is
    Initializable,
    ERC20Upgradeable,
    ERC20PausableUpgradeable,
    OwnableUpgradeable,
    UUPSUpgradeable
{
    address public treasuryContract;
    uint256 public maxTokenSupply;
    uint256 public fee; // This fee is in basis points, i.e., 100 means 1% fees
    mapping(address => bool) public isExcludeFromPayingFee;

    event TreasuryChanged(address indexed newTreasury);
    event FeeChanged(uint256 newFee);

    function initialize(
        string memory _name,
        string memory _symbol,
        address _treasuryContract,
        uint256 _maxTokenSupply,
        uint256 _fee
    ) public initializer {
        require(_treasuryContract != address(0), "Invalid treasury address");
        require(_maxTokenSupply != 0, "Invalid max token supply value");
        require(_fee != 0, "Invalid fees value");
        treasuryContract = _treasuryContract;
        fee = _fee;
        ERC20Upgradeable.__ERC20_init(_name, _symbol);
        __Ownable_init();
        transferOwnership(_treasuryContract);
        ERC20PausableUpgradeable.__ERC20Pausable_init();
        maxTokenSupply = _maxTokenSupply * (10 ** decimals());
        _mint(treasuryContract, maxTokenSupply);
    }

    function approve(
        address spender,
        uint256 amount
    ) public virtual override whenNotPaused returns (bool) {
        return super.approve(spender, amount);
    }

    function increaseAllowance(
        address spender,
        uint256 addedValue
    ) public virtual override whenNotPaused returns (bool) {
        return super.increaseAllowance(spender, addedValue);
    }

    function decreaseAllowance(
        address spender,
        uint256 subtractedValue
    ) public virtual override whenNotPaused returns (bool) {
        return super.decreaseAllowance(spender, subtractedValue);
    }

    function setTreasury(address _treasury) external whenNotPaused onlyOwner {
        require(_treasury != address(0), "Invalid treasury address");
        treasuryContract = _treasury;
        emit TreasuryChanged(_treasury);
    }

    function mint(
        address _userAddress,
        uint256 _amount
    ) public whenNotPaused onlyOwner {
        require(_userAddress != address(0), "Invalid user address");
        require(
            totalSupply() + _amount <= maxTokenSupply,
            "Exceeds max supply"
        ); // Check against maxSupply

        _mint(_userAddress, _amount);
    }

    function burn(uint256 _amount) public whenNotPaused onlyOwner {
        _burn(msg.sender, _amount);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override(ERC20Upgradeable, ERC20PausableUpgradeable) {
        ERC20PausableUpgradeable._beforeTokenTransfer(from, to, amount);
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}

    function setFee(uint256 _newFee) external whenNotPaused onlyOwner {
        require(_newFee <= 500, "Fee too high"); // ensuring fee doesn't exceed 50%
        fee = _newFee;
        emit FeeChanged(_newFee);
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal override {
        require(treasuryContract != address(0), "Treasury contract not set");
        require(recipient != address(0), "Invalid recipient address");

        if (
            isExcludeFromPayingFee[sender] || isExcludeFromPayingFee[recipient]
        ) {
            super._transfer(sender, recipient, amount); // transfer the full amount
        } else {
            uint256 commission = (amount * fee) / 10000; // calculate the fee
            uint256 netAmount = amount - commission;
            super._transfer(sender, recipient, netAmount); // transfer the net amount
            super._transfer(sender, treasuryContract, commission); // transfer the commission to the treasury
        }
    }

    function excludeAddressFromPayingFee(
        address _userAddress,
        bool _isExcluded
    ) public whenNotPaused onlyOwner {
        isExcludeFromPayingFee[_userAddress] = _isExcluded;
    }
}
