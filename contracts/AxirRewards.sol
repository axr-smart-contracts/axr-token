// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";



abstract contract AxirCustomERC20 {
    function totalSupply() public virtual view returns (uint256);
    function balanceOf(address account) public virtual view returns (uint256);
    function transfer(address recipient, uint256 amount) public virtual returns (bool);
    function allowance(address owner, address spender) public virtual view returns (uint256);
    function approve(address spender, uint256 amount) public virtual returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) public virtual returns (bool);
    function fee() public virtual returns (uint256);
    function decimals() public view virtual returns (uint8);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract AxirRewards is Initializable,OwnableUpgradeable, PausableUpgradeable, UUPSUpgradeable ,ReentrancyGuardUpgradeable   {
    using SafeERC20 for IERC20;
    address public tokenAddress;
    // events
    event UserRewarded(address indexed recipient, uint256 amount);
    event EmergencyWithdrawal(address owner,uint256 amount);
    event TokenAddressChanged(address oldTokenAddress,address newTokenAddress);

    // Initialize function for upgradeable contracts
    function initialize(address _tokenAddress) public initializer {
        require(_tokenAddress!=address(0), "AxirRewards: token address cannot be zero");
        tokenAddress=_tokenAddress;
         __Ownable_init();
        __Pausable_init();
        __UUPSUpgradeable_init();
        __ReentrancyGuard_init();
    }


    //  a function to reward users
    function rewardUsers(address[] memory _address,uint256[] memory _rewardAmount) external whenNotPaused onlyOwner nonReentrant  {
        require(_address.length>0,"AxirRewards: address array is empty");
        require(_rewardAmount.length>0,"AxirRewards: reward amount array is empty");
        require(_address.length==_rewardAmount.length,"AxirRewards: Address and Rewards array should be of same size");
        require(AxirCustomERC20(tokenAddress).balanceOf(address(this))>0,"AxirRewards: Contract token balance is zero");


        for(uint i;i<_address.length;++i){
            require(_address[i]!=address(0),"AxirRewards: recipient address cannot be zero");
            require(_rewardAmount[i]>0,"AxirRewards: recipient reward amount cannot be zero");

            IERC20(tokenAddress).safeTransfer(_address[i], _rewardAmount[i]);           
            emit UserRewarded(_address[i], _rewardAmount[i]);
        }
    }

    //  function to update the token address
    function updateTokenAddress(address _newTokenAddress) public whenNotPaused onlyOwner{
        require(_newTokenAddress!=address(0),"Invalid reward token address");
        IERC20 tokenContract = IERC20(_newTokenAddress);
        
    (bool successBalanceOf, bytes memory dataBalanceOf) = address(tokenContract).staticcall(abi.encodeWithSignature("balanceOf(address)", address(this)));
    require(successBalanceOf && dataBalanceOf.length > 0, "Invalid ERC20 Token Contract");

    address _oldTokenAddress=tokenAddress;
    tokenAddress = _newTokenAddress;
    emit TokenAddressChanged(_oldTokenAddress,_newTokenAddress);

    }

    // this function is used by owner to get back the unspent tokens
   function emergencyWithdraw( ) external  onlyOwner {
    uint256 balance = AxirCustomERC20(tokenAddress).balanceOf(address(this));
    require(balance>0,"AxirRewards: Contract token balance is zero");
    IERC20(tokenAddress).safeTransfer(owner(), balance);
    emit  EmergencyWithdrawal(owner(),balance);
    }

    // a function to pause the contract
    function pause() public onlyOwner {
       _pause();
    }


    // a function to unpause the contract
    function unpause() public onlyOwner {
       _unpause();
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}

}
