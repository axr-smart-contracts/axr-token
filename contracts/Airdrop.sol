// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

abstract contract AxirCustomERC20 {
    function totalSupply() public view virtual returns (uint256);

    function balanceOf(address account) public view virtual returns (uint256);

    function transfer(
        address recipient,
        uint256 amount
    ) public virtual returns (bool);

    function allowance(
        address owner,
        address spender
    ) public view virtual returns (uint256);

    function approve(
        address spender,
        uint256 amount
    ) public virtual returns (bool);

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual returns (bool);

    function fee() public virtual returns (uint256);

    function decimals() public view virtual returns (uint8);

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
}

contract Airdrop is
    Initializable,
    OwnableUpgradeable,
    PausableUpgradeable,
    UUPSUpgradeable,
    ReentrancyGuardUpgradeable
{
    using SafeERC20 for IERC20;
    address public tokenAddress;
    uint256 public airdropOptionId;
    uint256 public userCount;
    uint256 public currentAirdropOption;
    uint256 public currentUserRewardChangeCount;

    struct AirdropOption {
        uint256 rewardAmount;
        uint256 userCount;
    }
    mapping(uint256 => AirdropOption) public airdropOptionInfo;
    mapping(address => bool) public isUserRewarded;

    // events
    event AirdropOptionAdded(
        uint256 id,
        uint256 rewardAmount,
        uint256 userCount
    );
    event AirdropPerformed(address indexed recipient, uint256 amount);
    event EmergencyWithdrawal(address owner, uint256 amount);
    event TokenAddressChanged(address oldTokenAddress, address newTokenAddress);

    // Initialize function for upgradeable contracts
    function initialize(address _tokenAddress) public initializer {
        require(_tokenAddress != address(0));
        tokenAddress = _tokenAddress;
        __Ownable_init();
        __Pausable_init();
        __UUPSUpgradeable_init();
        __ReentrancyGuard_init();

        uint _tokenDecimals = AxirCustomERC20(tokenAddress).decimals();

        //    FOR PRODUCTION
        addAirdropOption(3000 * (10 ** _tokenDecimals), 1000);
        addAirdropOption(2500 * (10 ** _tokenDecimals), 9000);
        addAirdropOption(2000 * (10 ** _tokenDecimals), 40000);
        addAirdropOption(1000 * (10 ** _tokenDecimals), 50000);
        addAirdropOption(500 * (10 ** _tokenDecimals), 900000);
        currentAirdropOption = 0;
        currentUserRewardChangeCount = airdropOptionInfo[currentAirdropOption]
            .userCount;
    }

    //  a function to add airdrop option
    function addAirdropOption(
        uint256 _rewardAmount,
        uint256 _userCount
    ) public whenNotPaused onlyOwner {
        require(_rewardAmount != 0, "Invalid reward");
        require(_userCount != 0, "Invalid user count");
        AirdropOption memory newAirdropOption = AirdropOption({
            rewardAmount: _rewardAmount,
            userCount: _userCount
        });
        airdropOptionInfo[airdropOptionId] = newAirdropOption;
        airdropOptionId++;
        emit AirdropOptionAdded(airdropOptionId - 1, _rewardAmount, _userCount);
    }

    //  a function to perform airdrop
    function airdropToken(
        address[] memory _address
    ) external whenNotPaused onlyOwner nonReentrant {
        require(_address.length > 0, "Empty airdrop address array");
        for (uint i; i < _address.length; ++i) {
            userCount++;
            if (userCount > currentUserRewardChangeCount) {
                require(
                    currentAirdropOption < airdropOptionId,
                    "All airdrop phases ended"
                );
                currentAirdropOption++;
                currentUserRewardChangeCount += airdropOptionInfo[
                    currentAirdropOption
                ].userCount;
            }
            uint256 _rewardAmount = airdropOptionInfo[currentAirdropOption]
                .rewardAmount;
            require(!isUserRewarded[_address[i]], "User already rewarded");
            require(
                _address[i] != address(0),
                "Airdrop recipient address cannot be zero"
            );
            IERC20(tokenAddress).safeTransfer(_address[i], _rewardAmount);
            isUserRewarded[_address[i]] = true;

            emit AirdropPerformed(_address[i], _rewardAmount);
        }
    }

    //  function to update the token address
    function updateTokenAddress(
        address _newTokenAddress
    ) public whenNotPaused onlyOwner {
        require(
            _newTokenAddress != address(0),
            "Invalid airdrop token address"
        );
        IERC20 tokenContract = IERC20(_newTokenAddress);

        (bool successBalanceOf, bytes memory dataBalanceOf) = address(
            tokenContract
        ).staticcall(
                abi.encodeWithSignature("balanceOf(address)", address(this))
            );
        require(
            successBalanceOf && dataBalanceOf.length > 0,
            "Invalid ERC20 Token Contract"
        );

        address _oldTokenAddress = tokenAddress;
        tokenAddress = _newTokenAddress;
        emit TokenAddressChanged(_oldTokenAddress, _newTokenAddress);
    }

    // this function is used by owner to get back the unspent tokens
    function emergencyWithdraw() external onlyOwner {
        uint256 balance = AxirCustomERC20(tokenAddress).balanceOf(
            address(this)
        );
        IERC20(tokenAddress).safeTransfer(owner(), balance);
        emit EmergencyWithdrawal(owner(), balance);
    }

    // a function to pause the contract
    function pause() public onlyOwner {
        _pause();
    }

    // a function to unpause the contract
    function unpause() public onlyOwner {
        _unpause();
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}
}
