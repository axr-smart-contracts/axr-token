// require("@nomiclabs/hardhat-etherscan");
require("@nomicfoundation/hardhat-toolbox");
require("@openzeppelin/hardhat-upgrades");
require("dotenv").config();

/** @type import('hardhat/config').HardhatUserConfig */
const INFURA_API_KEY = process.env.INFURA_API_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;

module.exports = {
  solidity: "0.8.18",
  defaultNetwork: "hardhat",
  networks: {
    mumbai: {
      url: "https://polygon-mumbai.infura.io/v3/" + INFURA_API_KEY,
      accounts: ["0x" + PRIVATE_KEY],
      chainId: 80001,
      blockConfirmations: 6,
    },
    goerli: {
      url: "https://goerli.infura.io/v3/" + INFURA_API_KEY,
      accounts: ["0x" + PRIVATE_KEY],
      chainId: 5,
      blockConfirmations: 6,
    },
    polygon: {
      url: "https://polygon-mainnet.infura.io/v3/" + INFURA_API_KEY,
      accounts: ["0x" + PRIVATE_KEY],
      chainId: 137,
      blockConfirmations: 12,
      timeout: 120000, // Increase timeout to 2 minutes
      pollingInterval: 5000, // Check every 5 seconds
    },
    sepolia: {
      url: "https://sepolia.infura.io/v3/" + INFURA_API_KEY, // Sepolia endpoint
      accounts: ["0x" + PRIVATE_KEY],
      chainId: 11155111, // Sepolia chain ID
      blockConfirmations: 12, // You may adjust this based on Sepolia's block time
      timeout: 120000, // Increase timeout to 2 minutes
      pollingInterval: 5000, // Check every 5 seconds
    },
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  },
};
